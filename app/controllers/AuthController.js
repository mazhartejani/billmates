module.exports = {
    hello: function (req, res) {
        res.render('index', { title: 'Quick MVC' });
    },
    login: function (req, res) {
        res.render('login', { title: 'Login Page' });
    },
    dashboard: function (req, res) {
        res.render('dashboard',{title: 'Dashboard - BillMates', heading: 'Dashboard', isActive: 'dashboard', user: req.user});
    },
    signup: function (req, res) {
        res.render('signup', { title: 'Signup' });
    },
    profile: function (req, res) {
        res.render('profile',{title: 'User Profile - BillMates', heading: 'User Profile', isActive: 'profile', user: req.user});
    },
    activites: function (req, res) {
        res.render('activities',{title: 'Recent Activities - BillMates', heading: 'Recent Activities', isActive: 'activities', user: req.user});
    },
    expenses: function (req, res) {
        res.render('expenses',{title: 'Expenses - BillMates', heading: 'Expenses', isActive: 'expenses', user: req.user});
    },
    addExpense: function (req, res) {
        res.render('addExpense',{title: 'Add New Expense - BillMates', heading: 'Add New Expense', isActive: 'expenses', user: req.user});
    },
   
}